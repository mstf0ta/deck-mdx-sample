import theme from 'mdx-deck/themes'

export default {
  ...theme,

  colors: {
    text: 'yellow',
    background: '#19647E',
    link: 'yellow',
  },
  h1: {
    textTransform: 'uppercase',
    letterSpacing: '0.1em',
    color: 'white',
    textShadow: '5px 3px 2px rgba(150, 150, 150, 1)'

  },
  
  

  // Customize your presentation theme here.
  //
  // Read the docs for more info:
  // https://github.com/jxnblk/mdx-deck/blob/master/docs/theming.md
  // https://github.com/jxnblk/mdx-deck/blob/master/docs/themes.md

}
